const express = require('express')
const router = express.Router()
const cartController = require('../../controllers/cartController')

const validate = require('../../middlewares/cartValidate')

// Public
router.get('/', cartController.index)

// Private
router.get('/:user', cartController.show)

router.post('/', validate, cartController.store)

router.put('/', validate, cartController.update)

router.delete('/:user', cartController.destroy)

module.exports = router