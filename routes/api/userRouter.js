const express = require('express')
const router = express.Router()
const userController = require('../../controllers/userController')

const userValidate = require('../../middlewares/userValidate')
const sessionValidate = require('../../middlewares/sessionValidate')

// Public
router.post('/login', sessionValidate, userController.login)

router.get('/logout', userController.logout)

router.post('/', userValidate, userController.store)

// Private
router.get('/', userController.index)

router.get('/:id', userController.show)

router.put('/', userValidate, userController.update)

router.delete('/', userController.destroy)

module.exports = router