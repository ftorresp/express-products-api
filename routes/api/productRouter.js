const express = require('express')
const router = express.Router()
const productController = require('../../controllers/productController')

const validate = require('../../middlewares/productValidate')

// Public
router.get('/', productController.index)

router.get('/:id', productController.show)

// Private
router.post('/', validate, productController.store)

router.put('/', validate, productController.update)

router.delete('/', productController.destroy)

module.exports = router