const express = require('express')
const app = express()
const PORT = 5000
const cors = require('cors')
const apiRouter = require('./routes/api')
const morgan = require('morgan')

app.use(morgan('dev'))

app.use(cors())
app.use(express.json())

app.get('/', (req, res) => {
  res.send('Products API')
})

app.use('/api', apiRouter)

app.use((req, res) => {
  res.status(404).send('No se pudo encontrar')
})

app.listen(PORT, () => {
  console.log(`Server listens on port ${PORT}`)
})
