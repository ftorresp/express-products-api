openapi: 3.0.3

info:
  title: Products API - OpenAPI 3.0
  description: Esta es una API Rest de ejemplo que permite crear, ver, actualizar y eliminar usuarios, productos y carritos de compras asociados a los usuarios.
  version: 0.0.1
  license:
    name: MIT

servers:
  - url: http://localhost:5000/api
    description: Entorno de desarrollo

paths:

  /user:

    get:
      tags:
        - user
      summary: Retorna listado de usuarios.
      responses:
        '200':
          description: Un array de usuarios
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: number
                    createdAt:
                      type: string
                    lastModified:
                      type: string
                    firstname:
                      type: string
                    lastname:
                      type: string
                    email:
                      type: string
                    profilePicture:
                      type: string

    post:
      tags:
        - user
      summary: Crear un usuario
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                firstname:
                  type: string
                  minimum: 2
                lastname:
                  type: string
                  minimum: 2
                email:
                  type: string
                  format: email
                password:
                  type: string
                  minimum: 6
                profilePicture:
                  type: string
                  nullable: true
              required: ['firstname', 'lastname', 'email', 'password']
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: number
                  createdAt:
                    type: string
                  lastModified:
                    type: string
                  firstname:
                    type: string
                  lastname:
                    type: string
                  email:
                    type: string
                  profilePicture:
                    type: string
        '405':
          description: Petición invalida
        '500':
          description: Error en el servidor

    put:
      tags:
        - user
      summary: Actualizar un usuario
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                id:
                  type: number
                  minimum: 0
                firstname:
                  type: string
                  minimum: 2
                lastname:
                  type: string
                  minimum: 2
                email:
                  type: string
                  format: email
                password:
                  type: string
                  minimum: 6
                profilePicture:
                  type: string
                  nullable: true
              required: ['id']
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: number
                  createdAt:
                    type: string
                  lastModified:
                    type: string
                  firstname:
                    type: string
                  lastname:
                    type: string
                  email:
                    type: string
                  profilePicture:
                    type: string
        '400':
          description: El id está undefined
        '404':
          description: El usuario no existe
        '405':
          description: Petición invalida
        '500':
          description: Error en el servidor

    delete:
      tags:
        - user
      summary: Eliminar un usuario
      parameters:
        - in: query
          name: id
          schema:
            type: number
          required: true
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: number
                  createdAt:
                    type: string
                  lastModified:
                    type: string
                  firstname:
                    type: string
                  lastname:
                    type: string
                  email:
                    type: string
                  profilePicture:
                    type: string
        '400':
          description: El id está undefined
        '404':
          description: El usuario no existe
        '500':
          description: Error en el servidor

  /user/{id}:

    get:
      tags:
        - user
      summary: Retorna un usuarios.
      parameters:
        - in: path
          name: id
          schema:
            type: number
          required: true
      responses:
        '200':
          description: Un usuario
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: number
                  createdAt:
                    type: string
                  lastModified:
                    type: string
                  firstname:
                    type: string
                  lastname:
                    type: string
                  email:
                    type: string
                  profilePicture:
                    type: string
        '400':
          description: El id está undefined
        '404':
          description: El usuario no existe
        '500':
          description: Error en el servidor

  /user/login:

    post:
      tags:
        - user
      summary: Iniciar sesión
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                password:
                  type: string
              required: ['email', 'password']
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: number
                  name:
                    type: string
                  email:
                    type: string
        '400':
          description: El usuario y la contraseña no coinciden
        '500':
          description: Error en el servidor

  /user/logout:

    get:
      tags:
        - user
      summary: Cerrar sesión
      parameters:
        - in: query
          name: email
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Sesión finalizada
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: number
                  name:
                    type: string
                  email:
                    type: string
        '400':
          description: El parametro email es requerido
        '404':
          description: La sesión no existe
        '500':
          description: Error en el servidor



  /product:

    get:
      tags:
        - product
      summary: Retorna listado de productos.
      responses:
        '200':
          description: Un array de productos
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    createdAt:
                      type: string
                    lastModified:
                      type: string
                    title:
                      type: string
                    price:
                      type: number
                    stock:
                      type: number
                    description:
                      type: string
                    category:
                      type: string
                    images:
                      type: array
                      items:
                        type: string
                    rating:
                      type: object
                      properties:
                        rate:
                          type: number
                        count:
                          type: integer

    post:
      tags:
        - product
      summary: Crear un producto
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                title:
                  type: string
                  minLength: 2
                price:
                  type: number
                  minimum: 0
                stock:
                  type: number
                  minimum: 0
                description:
                  type: string
                category:
                  type: string
                images:
                  type: array
                  items:
                    type: string
                rating:
                  type: object
                  properties:
                    rate:
                      type: number
                      minimum: 0
                      maximum: 5
                    count:
                      type: integer
                      minimum: 0
              required: ['title', 'price', 'description']
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  createdAt:
                    type: string
                  lastModified:
                    type: string
                  title:
                    type: string
                  price:
                    type: number
                  description:
                    type: string
                  stock:
                    type: number
                  category:
                    type: string
                  images:
                    type: array
                    items:
                      type: string
                  rating:
                    type: object
                    properties:
                      rate:
                        type: number
                      count:
                        type: integer
        '400':
          description: Petición invalida
        '500':
          description: Error en el servidor

    put:
      tags:
        - product
      summary: Actualizar un producto
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                id:
                  type: number
                  minimum: 0
                title:
                  type: string
                  minLength: 2
                price:
                  type: number
                  minimum: 0
                stock:
                  type: number
                  minimum: 0
                description:
                  type: string
                category:
                  type: string
                images:
                  type: array
                  items:
                    type: string
                rating:
                  type: object
                  properties:
                    rate:
                      type: number
                      minimum: 0
                      maximum: 5
                    count:
                      type: integer
                      minimum: 0
              required: ['id']
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  createdAt:
                    type: string
                  lastModified:
                    type: string
                  title:
                    type: string
                  price:
                    type: number
                  stock:
                    type: number
                  description:
                    type: string
                  category:
                    type: string
                  images:
                    type: array
                    items:
                      type: string
                  rating:
                    type: object
                    properties:
                      rate:
                        type: number
                      count:
                        type: integer
        '400':
          description: El id está undefined
        '405':
          description: Petición invalida
        '500':
          description: Error en el servidor

    delete:
      tags:
        - product
      summary: Eliminar un producto
      parameters:
        - in: query
          name: id
          schema:
            type: number
          required: true
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  createdAt:
                    type: string
                  lastModified:
                    type: string
                  title:
                    type: string
                  price:
                    type: number
                  stock:
                    type: number
                  description:
                    type: string
                  category:
                    type: string
                  images:
                    type: array
                    items:
                      type: string
                  rating:
                    type: object
                    properties:
                      rate:
                        type: number
                      count:
                        type: integer
        '400':
          description: El id está undefined
        '404':
          description: El producto no existe
        '500':
          description: Error en el servidor

  /product/{id}:

    get:
      tags:
        - product
      summary: Retorna un producto.
      parameters:
        - in: path
          name: id
          schema:
            type: number
          required: true
      responses:
        '200':
          description: Un producto
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  createdAt:
                    type: string
                  lastModified:
                    type: string
                  title:
                    type: string
                  price:
                    type: number
                  stock:
                    type: number
                  description:
                    type: string
                  category:
                    type: string
                  images:
                    type: array
                    items:
                      type: string
                  rating:
                    type: object
                    properties:
                      rate:
                        type: number
                      count:
                        type: integer
        '400':
          description: El id está undefined
        '404':
          description: El producto no existe
        '500':
          description: Error en el servidor



  /cart:

    post:
      tags:
        - cart
      summary: Agrega un producto a un carrito
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                userId:
                  type: integer
                  minimum: 0
                product:
                  type: object
                  properties:
                    id:
                      type: integer
                      minimum: 0
                  required: ['id']
              required: ['userId', 'product']
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    quantity:
                      type: integer
        '400':
          description: El id del usuario está undefined
        '404':
          description: El usuario o el producto no existen
        '405':
          description: Petición invalida
        '500':
          description: Error en el servidor

    put:
      tags:
        - cart
      summary: Actualizar la cantidad de un producto en un carrito
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                userId:
                  type: integer
                  minimum: 0
                product:
                  type: object
                  properties:
                    id:
                      type: integer
                      minimum: 0
                    quantity:
                      type: integer
                      minimum: 0
                  required: ['id', 'quantity']
              required: ['userId', 'product']
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    quantity:
                      type: integer
        '400':
          description: El id del usuario está undefined
        '404':
          description: El usuario o el producto no existen
        '405':
          description: Petición invalida
        '500':
          description: Error en el servidor

    delete:
      tags:
        - cart
      summary: Elimina un producto de un carrito
      parameters:
        - in: path
          name: userId
          schema:
            type: integer
          required: true
        - in: query
          name: productId
          schema:
            type: integer
          required: true
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    quantity:
                      type: integer
        '400':
          description: El id del usuario está undefined
        '404':
          description: El usuario o el producto no existen
        '405':
          description: Petición invalida
        '500':
          description: Error en el servidor

  /cart/{userId}:

    get:
      tags:
        - cart
      summary: Retorna un carrito.
      parameters:
        - in: path
          name: userId
          schema:
            type: number
          required: true
      responses:
        '200':
          description: Un carrito
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    quantity:
                      type: integer
        '400':
          description: El id del usuario está undefined
        '404':
          description: El carrito no existe
        '500':
          description: Error en el servidor