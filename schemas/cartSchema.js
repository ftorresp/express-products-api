const schema = {
  type: 'object',

  properties: {
    userId: { 
      type: 'integer', 
      minimum: 0
    },

    product: { 
      type: 'object',
      
      properties: {
        id: {
          type: 'integer',
          minimum: 0
        },
        quantity: {
          type: 'integer',
          minimum: 0
        }
      },

      required: ['id'],
      
      additionalProperties: false,

      errorMessage: {
        properties: {
          id: "El campo id debe ser un entero mayor o igual a cero.",
          quantity: "El campo quantity debe ser un entero mayor o igual a cero."
        },
        required: {
          id: "El campo id de product es requerido.",
          quantity: "El campo quantity de product es requerido.",
        }
      }
    },
  },

  additionalProperties: false,

  errorMessage: {
    type: 'El carrito debe ser de tipo objeto.',
    additionalProperties: "No debe contener propiedades adicionales",
    properties: {
      userId: "El campo userId debe ser de tipo entero mayor o igual a cero.",
      product: "El campo product debe ser de tipo objeto."
    },
    required: {
      userId: "El campo userId es requerido.",
      product: "El campo product es requerido."
    }
  }
}

module.exports = schema