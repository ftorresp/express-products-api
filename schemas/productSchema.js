const schema = {
  type: 'object',

  properties: {
    id: { 
      type: 'integer', 
      minimum: 0
    },
    isActive: {
      type: 'boolean'
    },
    createdAt: { 
      type: 'string',
      format: 'date-time'
    },
    lastModified: { 
      type: 'string',
      format: 'date-time'
    },
    title: { 
      type: 'string',
      minLength: 2
    },
    price: { 
      type: 'integer',
      minimum: 0
    },
    description: { 
      type: 'string'
    },
    category: { 
      type: 'string'
    },
    stock: {
      type: 'integer',
      minimum: 0
    },
    images: { 
      type: 'array',
      items: {
        type: 'string',
        errorMessage: "Los elementos de images deben ser de tipo string."
      },
    },
    rating: {
      type: 'object',
      properties: {
        rate: {
          type: 'number',
          minimum: 0,
          maximum: 5,
          errorMessage: "El campo rate debe contener un entero entre 0 y 5."
        },
        count: {
          type: 'integer',
          minimum: 0,
          errorMessage: "El campo count debe contener un entero mayor o igual a cero."
        }
      }
    }
  },

  additionalProperties: false,

  errorMessage: {
    type: "El producto debe ser de tipo objeto.",
    additionalProperties: "No debe contener propiedades adicionales",
    properties: {
      id: "El campo id debe ser de tipo entero mayor o igual a cero.",
      title: "El campo title debe ser un string de mínimo dos caracteres.",
      price: "El campo price debe ser un entero mayor o igual a cero.",
      description: "El campo description debe ser de tipo string.",
      category: "El campo category debe ser de tipo string.",
      images: "El campo images debe ser de tipo array y contener strings.",
      rating: "El campo rating debe ser de tipo objeto.",
      stock: "El campo stock debe ser un entero mayor o igual a cero."
    },
    required: {
      id: "El campo id es requerido",
      title: "El campo title es requerido",
      price: "El campo price es requerido",
      description: "El campo description es requerido",
    }
  }
}

module.exports = schema