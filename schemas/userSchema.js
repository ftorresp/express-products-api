const schema = {
  type: 'object',

  properties: {
    id: { 
      type: 'integer', 
      minimum: 0
    },
    isActive: {
      type: 'boolean'
    },
    createdAt: { 
      type: 'string',
      format: 'date-time'
    },
    lastModified: { 
      type: 'string',
      format: 'date-time'
    },
    firstname: { 
      type: 'string',
      minLength: 2
    },
    lastname: { 
      type: 'string',
      minLength: 2
    },
    email: { 
      type: 'string',
      format: 'email'
    },
    password: { 
      type: 'string',
      minLength: 6
    },
    profilePicture: { 
      type: 'string',
      nullable: true
    }
  },

  additionalProperties: false,

  errorMessage: {
    type: "El usuario debe ser de tipo objeto.",
    additionalProperties: "No debe contener propiedades adicionales",
    properties: {
      id: "El campo id debe ser de tipo entero mayor o igual a cero.",
      firstname: "El campo firstname debe ser un string de mínimo dos caracteres.",
      lastname: "El campo lastname debe ser un string de mínimo dos caracteres.",
      email: "El campo email debe ser un string con formato de email.",
      password: "El campo password debe ser un sting de mínimo 6 caracteres.",
      profilePicture: "El campo profilePicture debe ser de tipo string.",
    },
    required: {
      id: "El campo id es requerido.",
      firstname: "El campo firstname es requerido.",
      lastname: "El campo lastname es requerido.",
      email: "El campo email es requerido.",
      password: "El campo password es requerido.",
    }
  }
}

module.exports = schema