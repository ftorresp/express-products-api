# Products API
Esta es una API Rest de productos de ejemplo, construida con el objetivo de facilitar la práctica de desarrollo de aplicaciones de frontend que requieran consumir una API.

La API permite realizar operaciones CRUD sobre las entidades User, Product y Cart.

## Instalación
Las siguientes instrucciones permiten instalar y ejecutar la API.
```
git clone https://gitlab.com/ftorresp/express-products-api.git products-api
cd products-api
npm install
npm start
```
Por defecto correrá en el puerto 5000

## Documentación
La documentación se encuentra disponible en el siguiente enlace [Documentación](https://gitlab.com/ftorresp/express-products-api/-/blob/dev/swagger.yaml#/)

También es posible acceder a [editor.swagger.io](https://editor.swagger.io) y remplazar el código que aparece por defecto por el que se encuentraen el archivo swagger.yaml que se encuentra en la raíz de este proyecto.