const { getItems, getItemById, storeItem, updateItem, destroyItem, respondWithInvalidRequest, respondWithInternalError } = require('../lib/utils')
const { getFile, createProduct } = require('../lib/product')

const file = getFile()

const index = (req, res) => {
  const products = getItems(file)
  res.status(200).json(products)
}

const show = (req, res) => {
  try {
    if (req.params.id !== undefined) {
      const id = Number(req.params.id)

      const reg = getItemById(id, file)
      if (reg) {
        return res.status(200).json(reg)
      }

      return respondWithInvalidRequest(res, 'El producto no existe.', 404)
    }

    respondWithInvalidRequest(res, 'El id está undefined')
    
  } catch (e) {
    respondWithInternalError(e, res)
  }
}

const store = (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  try {
    const products = getItems(file)
    const product = createProduct(req.body, products);
    const reg = storeItem(product, file)
    res.status(201).json(reg)

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

const update = (req, res) => { 
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  try {
    const product = {...req.body}
    if (product.id !== undefined) {
      product.id = Number(product.id)
      product.lastModified = new Date
      
      if (getItemById(product.id, file)) { 
        const reg = updateItem(product, file)
        return res.status(200).json(reg)
      }
      
      return respondWithInvalidRequest(res, 'El producto no existe.', 404)
    }

    return respondWithInvalidRequest(res, 'El id está undefined', 400)

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

const destroy = (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  try {
    if (req.query.id !== undefined) {
      const id = Number(req.query.id)
      
      if (getItemById(id, file)) {
        const reg = destroyItem(id, file)
        return res.status(200).json(reg)
      }

      return respondWithInvalidRequest(res, 'El producto no existe.', 404)
    }

    respondWithInvalidRequest(res, 'El id está undefined')

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

module.exports = {
  index,
  show,
  store,
  update,
  destroy
}