const { getItems, storeItem, updateItem, respondWithInvalidRequest, respondWithInternalError } = require('../lib/utils')
const { getFile, getCartByUserId, createCart } = require('../lib/cart')
const { getUserById } = require('../lib/user')
const { getProductById } = require('../lib/product')

const file = getFile()

const index = (req, res) => {
  const carts = getItems(file)
  res.status(200).json(carts)
}

const show = async (req, res) => {
  try {
    const userId = Number(req.params.user)
    if (userId !== undefined) {

      const reg = getCartByUserId(userId)
      if (reg) {

        let flag = false
        const cart = reg.products.filter(p => {
          if (getProductById(p.id)) {
            return true
          }
          flag = true
        })

        if (flag) {
          reg.products = [...cart]
          updateItem(reg, file, userId)
        }

        return res.status(200).json(reg.products)
      }

      return respondWithInvalidRequest(res, 'El carrito no existe.', 404)
    }

    respondWithInvalidRequest(res, 'El id del usuario está undefined')
    
  } catch (e) {
    respondWithInternalError(e, res)
  }
}

const store = async (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  try {
    const userId = req.body.userId

    if (userId !== undefined) {

      if (getUserById(userId)) { 

        const { product } = req.body

        if (getProductById(product.id)) {

          const userCart = getCartByUserId(userId)
          if (userCart !== undefined) { // El usuario ya tiene una carta


            const index = userCart.products.findIndex(p => p.id === product.id)
            
            if (index > -1) { // El producto existe  
              userCart.products[index].quantity += 1
            } else { // El producto no existe
              userCart.products.push({ id: product.id, quantity: 1 })
            }
            
            const reg = updateItem(userCart, file, 'userId')
            return res.status(200).json(reg.products)      
          } 
          
          const newCart = createCart(req.body);
          const reg = storeItem(newCart, file, 'userId')
          return res.status(200).json(reg.products)
        }

        return respondWithInvalidRequest(res, 'El producto no existe.')
      }

      return respondWithInvalidRequest(res, 'El usuario no existe.')
    }

    respondWithInvalidRequest(res, 'El id del usuario está undefined')

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

const update = async (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  try {
    if (req.body.userId !== undefined) {
      const userId = Number(req.body.userId)
      
      const currentCart = getCartByUserId(userId)
      
      if (currentCart) {       
        
        const { product } = req.body

        const index = currentCart.products.findIndex(p => p.id === product.id)

        if (index > -1) {
          currentCart.lastModified = new Date
          currentCart.products[index].quantity = product.quantity
          const reg = updateItem(currentCart, file, 'userId')
          return res.status(200).json(reg.products)
        }
        
        return respondWithInvalidRequest(res, 'El producto no existe en el carrito.', 400)
      }
      
      return respondWithInvalidRequest(res, 'El usuario no tiene carrito.', 400)
    }

    respondWithInvalidRequest(res, 'El id del usuario está undefined')

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

const destroy = async (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  try {
    if (req.params.user !== undefined) {
      const userId = Number(req.params.user)
      
      const cart = getCartByUserId(userId)
      if (cart !== undefined) {

        const { productId } = req.query

        const index = cart.products.findIndex(p => p.id === Number(productId))

        if (index > -1) {
          cart.products.splice(index, 1)
          const reg = updateItem(cart, file, 'userId')
          return res.status(200).json(reg.products)
        }

        return respondWithInvalidRequest(res, 'El producto no existe en el carrito.', 404)  
      }

      return respondWithInvalidRequest(res, 'El carrito no existe.', 404)
    }

    respondWithInvalidRequest(res, 'El id del usuario está undefined')

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

module.exports = {
  index,
  show,
  store,
  update,
  destroy
}