const bcrypt = require('bcrypt')
const { createUser, getFile, getUsers, getUserByEmail, getUserPassword } = require('../lib/user')
const { getItemById, storeItem, updateItem, destroyItem, respondWithInvalidRequest, respondWithInternalError } = require('../lib/utils')
const { getFile: getSessionFile, createSession, getSessionByEmail } = require('../lib/session')

const file = getFile()
const sessionFile = getSessionFile()

const index = (req, res) => {
  const users = getUsers()
  res.status(200).json(users)
}

const show = (req, res) => {
  try {
    if (req.params.id !== undefined) {
      const id = Number(req.params.id)

      const reg = getItemById(id, file)
      if (reg) {
        delete reg.password
        return res.status(200).json(reg)
      }

      return respondWithInvalidRequest(res, 'El usuario no existe', 404)
    }

    return respondWithInvalidRequest(res, 'El id está undefined')
    
  } catch (e) {
    respondWithInternalError(e, res)
  }
}

const store = async (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  try {
    const users = getUsers()

    const userExists = users.find(u => u.email === req.body.email)
    
    if (userExists)
      return respondWithInvalidRequest(res, 'El correo electrónico ya existe.', 409)

    req.body.password = await bcrypt.hash(req.body.password, 10)

    const user = createUser(req.body, users);
    const reg = storeItem(user, file)
    delete reg.password
    res.status(201).json(reg)

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

const update = (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  try {
    const user = {...req.body}
    if (user.id !== undefined) {
      user.id = Number(user.id)
      user.lastModified = new Date

      if (getItemById(user.id, file)) {
        const reg = updateItem(user, file)
        delete reg.password
        return res.status(200).json(reg)
      }
      
      return respondWithInvalidRequest(res, 'El usuario no existe', 404)
    }

    return respondWithInvalidRequest(res, 'El id está undefined')

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

const destroy = (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)
  
  try {
    if (req.query.id !== undefined) {
      const id = Number(req.query.id)
      
      if (getItemById(id, file)) {
        const reg = destroyItem(id, file)
        delete reg.password
        return res.status(200).json(reg)
      }

      return respondWithInvalidRequest(res, 'El usuario no existe', 404)
    }

    return respondWithInvalidRequest(res, 'El id está undefined')

  } catch (e) {
    return respondWithInternalError(e, res)
  }
}

const login = async (req, res) => {
  if (req.errors) return respondWithInvalidRequest(res, req.errors, 405)

  const { email, password } = req.body

  const user = getUserByEmail(email)
  
  if ( user !== undefined ) {

    const passwordHash = getUserPassword(user.id)

    const match = await bcrypt.compare(password, passwordHash)
    
    if (match) {

      let session = getSessionByEmail(email)

      if (session) {
        session.lastModified = new Date
        const reg = updateItem(session, sessionFile)
        return res.json({ id: reg.id , name: reg.user, email: reg.email })
      }

      session = createSession(user)

      const reg = storeItem(session, sessionFile)
    
      return res.json({ id: reg.id , name: reg.user, email: reg.email })

    }
  }

  return respondWithInvalidRequest(res, 'El usuario y la contraseña no coinciden.')
}

const logout = (req, res) => {
  const { email } = req.query

  if (!req.query.email) 
    return respondWithInvalidRequest(res, 'El parametro email es requerido')

  const session = getSessionByEmail(email)

  if (session) {
    const reg = destroyItem(email, sessionFile, 'email')
    if (reg) return res.status(200).json({ reg })
  }

  return respondWithInvalidRequest(res, 'La sesión no existe.')
}

module.exports = {
  index,
  show,
  store,
  update,
  destroy,
  login,
  logout
}