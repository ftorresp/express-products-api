const Ajv = require('ajv').default
const addFormats = require('ajv-formats')
const ajv = new Ajv({allErrors: true})
const schema = require('../schemas/userSchema')
require("ajv-errors")(ajv)

addFormats(ajv)

const userValidate = (req, res, next) => {

  const validate = ajv.compile({
    ...schema,
    required: ['email', 'password']
  })
  
  const valid = validate(req.body)
  
  if (!valid) req.errors = validate.errors[0].message
  
  next()
}

module.exports = userValidate