const Ajv = require('ajv').default
const addFormats = require('ajv-formats')
const ajv = new Ajv({allErrors: true})
const schema = require('../schemas/userSchema')
require("ajv-errors")(ajv /*, {singleError: true}*/)

addFormats(ajv)

const userValidate = (req, res, next) => {

  const { method } = req
  let required = []

  if (method === 'POST') 
    required = ['firstname', 'email', 'password']

  if (method === 'PUT')
    required = ['id']

  // if (method === 'DELETE')
  //   required = ['id']

  const validate = ajv.compile({
    ...schema,
    required
  })
  
  const valid = validate(req.body)
  
  if (!valid) req.errors = validate.errors[0].message
  
  next()
}

module.exports = userValidate