const { respondWithInvalidRequest } = require('../lib/utils')
const { getSessionByEmail } = require('../lib/session')

const auth = (req, res, next) => {

  const { email } = req.headers

  if (email === undefined)
    return respondWithInvalidRequest(res, 'Este recurso requiere autenticación.')

  const session = getSessionByEmail(email)

  if (!session)
    return respondWithInvalidRequest(res, 'El usuario no ha iniciado sesión.')

  next()
}

module.exports = auth