const Ajv = require('ajv').default
const addFormats = require('ajv-formats')
const ajv = new Ajv({allErrors: true})
const schema = require('../schemas/cartSchema')
require("ajv-errors")(ajv)

addFormats(ajv)

const cartValidate = (req, res, next) => {

  const { method } = req
  let required = []

  if (method === 'POST') {
    schema.properties.product.required = ['id']
    required = ['userId', 'product']
  }

  if (method === 'PUT') {
    // Acá no anda con required.push('quantity') 
    // Arroja "must NOT have duplicate"
    schema.properties.product.required = ['id','quantity']
    required = ['userId', 'product']
  }

  const validate = ajv.compile({
    ...schema,
    required
  })

  const valid = validate(req.body)
  
  if (!valid) req.errors = validate.errors[0].message
  
  next()
}

module.exports = cartValidate