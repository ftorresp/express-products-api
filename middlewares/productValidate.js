const Ajv = require('ajv').default
const addFormats = require('ajv-formats')
const ajv = new Ajv({allErrors: true})
const schema = require('../schemas/productSchema')
require("ajv-errors")(ajv)

addFormats(ajv)

const validate = (req, res, next) => {

  const { method } = req
  let required = []

  if (method === 'POST') 
    required = ['title', 'price', 'description']

  if (method === 'PUT')
    required = ['id']

  const validate = ajv.compile({
    ...schema,
    required
  })
  
  const valid = validate(req.body)
  
  if (!valid) req.errors = validate.errors[0].message
  
  next()
}

module.exports = validate