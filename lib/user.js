const { createItemId, getItems, getItemById } = require('./utils')
const path = require('path')


const getFile = () => 
  path.join(__dirname, '../data/users.json')


const getUsers = () =>
  getItems(getFile()).map(u => {
    delete u.password
    return u
  })


const getUserById = (id) => 
  getItemById(id, getFile())


const getUserByEmail = (email) => 
  getUsers().find(u => u.email === email)


const getUserPassword = (id) => {
  const users = getItems(getFile())
  const { password } = users.find(u => u.id === id)
  return password
}


const createUser = (data, users) => {
  const date = new Date
  return {
    id: createItemId(users),
    isActive: true,
    createdAt: date,
    lastModified: date,
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    profilePicture: '',
    ...data
  }
}


module.exports = {
  createUser,
  getUsers,
  getUserById,
  getUserByEmail,
  getUserPassword,
  getFile
}