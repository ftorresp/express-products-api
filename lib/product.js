const { createItemId, getItemById } = require('./utils')
const path = require('path')


const getFile = () => 
  path.join(__dirname, '../data/products.json')


const getProductById = (id) => 
  getItemById(id, getFile())


const createProduct = (data, products) => {
  const date = new Date
  return {
    id: createItemId(products),
    isActive: true,
    createdAt: date,
    lastModified: date,
    title: '',
		price: 0,
		description: '',
		category: '',
		images: [],
    stock: 0,
		rating: {
			rate: 0,
			count: 0
		},
    ...data
  }
}


module.exports = {
  getFile,
  getProductById,
  createProduct
}