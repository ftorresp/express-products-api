const path = require('path')
const { getItemById } = require('./utils')


const getFile = () => 
  path.join(__dirname, '../data/carts.json')


const getCartByUserId = (userId) => 
  getItemById(userId, getFile(), 'userId')


const createCart = ({ userId, product }) => {
  const date = new Date
  return {  
    userId,
    createdAt: date,
    lastModified: date,
    products: [{...product, quantity: 1}]
  }
}


module.exports = {
  getFile,
  getCartByUserId,
  createCart
}