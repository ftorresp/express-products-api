const { readFile, writeFile } = require("../services/filesystem")


const createItemId = (items, field='id') => {
  if (!items.length) return 0
  const ids = items.map(n => n[field])
  const max = Math.max(...ids)
  return max + 1
}

const getItems = (file) => {
  return readFile(file)
}

const getItemById = (id, file, field='id') => {
  const items = getItems(file)
  return items.find(n => n[field] === Number(id))
}

const storeItem = (newItem, file) => {
  const currentItems = getItems(file)
  const updatedItems = [...currentItems, newItem]
  if (writeFile(file, updatedItems)) return newItem
}

const updateItem = (item, file, field='id') => {
  const oldItem = getItemById(item[field], file)
  const newItem = {...oldItem, ...item}
  const items = getItems(file)
  const updatedItems = items.map(u => u[field] === item[field] ? newItem : u )
  if (writeFile(file, updatedItems)) return newItem
}

const destroyItem = (id, file, field='id') => {
  const items = getItems(file)
  const index = items.findIndex(u => u[field] === id)
  const updatedItems = [...items]
  const deletedItem = updatedItems.splice(index, 1)
  if (writeFile(file, updatedItems)) return deletedItem[0]
}

const respondWithNotFound = (res, message, status=404) => {
  return res.status(status).json({
    error: 404,
    message
  })
}

const respondWithInvalidRequest = (res, e, status=400) => {
  return res.status(status).json({
    status,
    error: e,
  })
}

const respondWithInternalError = (e, res, status=500) => {
  console.log(e)
  return res.status(status).send({
    message: 'Ha ocurrido un error.'
  })
}

module.exports = {
  createItemId,
  getItems,
  getItemById,
  storeItem,
  updateItem,
  destroyItem,
  respondWithNotFound,
  respondWithInvalidRequest,
  respondWithInternalError
}