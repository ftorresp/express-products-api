const { getItems } = require('./utils')
const path = require('path')


const getFile = () => 
  path.join(__dirname, '../data/sessions.json')


const getSessionByEmail = (email) =>
  getItems(getFile()).find(s => s.email === email)


const createSession = ({ id, email, firstname }) => {
  const date = new Date
  return {
    createdAt: date,
    lastModified: date,
    id,
    email,
    user: firstname
  }
}


module.exports = {
  createSession,
  getSessionByEmail,
  getFile
}